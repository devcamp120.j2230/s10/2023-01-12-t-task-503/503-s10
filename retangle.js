 // Khai báo 1 class hình chữ nhật
 class Retangle {
    // Khai báo các thuộc tính của class hình chữ nhật
    width;
    height;

    // Khai báo phương thức khởi tạo cho class
    constructor(paramWidth, paramHeight) {
        this.width = paramWidth;
        this.height = paramHeight;
    }

    // Khai báo các phương thức thêm cho class
    getArea() {
        return this.width * this.height;
    }
}

export { Retangle }