import { Retangle } from "./retangle.js";
import { Square } from "./square.js";

var retangle1 = new Retangle(10, 5);
console.log(retangle1.width);
console.log(retangle1.height);
console.log(retangle1.getArea());
var retangle2 = new Retangle(20, 10);
console.log(retangle2.width);
console.log(retangle2.height);
console.log(retangle2.getArea());

var square1 = new Square(5, "Đây là hình vuông");
console.log(square1.width);
console.log(square1.height);
console.log(square1.description);
console.log(square1.getArea());
console.log(square1.getPerimeter());
var square2 = new Square(10);
console.log(square2.width);
console.log(square2.height);
console.log(square2.description);
console.log(square2.getArea());
console.log(square2.getPerimeter());

console.log(retangle1 instanceof Retangle); //T 
console.log(retangle1 instanceof Square); //F
console.log(square1 instanceof Retangle); //T
console.log(square1 instanceof Square); //T

// Hoisting
a = 1;
console.log(a);
var a;