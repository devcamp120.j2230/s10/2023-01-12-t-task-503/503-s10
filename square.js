import { Retangle } from "./retangle.js";

// Khai báo 1 class hình vuông (hình vuông có width và height, hình vuông có getArea)
class Square extends Retangle {
    // Khai báo các thuộc tính thêm của hình vuông mà hình chữ nhật không có
    description;

    // Khai báo phương thức khởi tạo cho hình vuông
    constructor(paramLength, paramDescription) {
        // Sử dụng super để gắn giá trị cho width và height
        super(paramLength, paramLength);

        this.description = paramDescription;
    }

    // Khai báo các phương thức thêm của hình vuông mà hình chữ nhật không có
    getPerimeter() {
        return 4 * this.height;
    }
}

export { Square }